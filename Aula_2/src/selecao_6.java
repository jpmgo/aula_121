import java.util.Scanner;

public class selecao_6 {
	
	static Scanner entrada = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int valor;
		
		System.out.println("Digite um valor: ");
		valor = entrada.nextInt();
		
		if (valor >0 ) {
			System.out.println("Positivo");
		}
		else if (valor < 0) {
			System.out.println("Negativo");
		}
		else {
			System.out.println("Zero");
		}
	}
	

}

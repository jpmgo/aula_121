import java.util.Scanner;

public class exercicio_1 {

	static Scanner tecla = new Scanner(System.in);
			
	public static void main(String[] args) {
		
	//Declara��o de vari�veis	
		double raio, area;
		final double PI = 3.14;
		
	//Entrada de dados	
		System.out.println("Digite o valor do raio: ") ;
		raio = tecla.nextDouble();
		
	//Processamento de dados	
		area = PI * Math.pow(raio,2);

	//Sa�da de dados	
		System.out.println("A �rea do c�rculo �: "+area);
		
	}

}

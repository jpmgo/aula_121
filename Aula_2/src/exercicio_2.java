import java.util.Scanner;

public class exercicio_2 {

	static Scanner entrada = new Scanner(System.in);
	
	public static void main(String[] args) {
	
     //Declara��o de vari�veis		
		double celsius, fahrenheit;
		
	//Entrada de dados	
		System.out.println("Qual � a temperatura em fahrenheit: ");
		fahrenheit = entrada.nextDouble();
		
	//Processamento de dados	
		celsius = ((fahrenheit - 32)/9) *5;
		
		
	//Sa�da de dados	
		System.out.println("A temperatura em Celsius �: "+ celsius);

	}

}
